<?php 
require_once __DIR__."/vendor/autoload.php";

use Hekmatinasser\Verta\Verta;

$v=new Verta();

 $v->timezone('Asia/Tehran');

echo $v->now();

echo $v->format('%B %d، %Y');  